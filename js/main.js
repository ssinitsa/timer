let addZero = number =>
  number < 10 ? `0${number}` : `${number}`;

let startTimer = () => {
  timerID = setInterval(() => {
    ms++;
    if (ms === 100) {
      s++;
      ms = 0;
    }
    if (s === 60) {
      m++;
      s = 0;
    }
    if (m === 60) m = 0;
    updateTimer();
  }, 10);

  startBtn.style.display = 'none';
  pauseBtn.style.display = 'inline-block';
};

let updateTimer = () => {
  document.getElementById('m-seconds').innerText = `${addZero(ms)}`;
  document.getElementById('seconds').innerText = `${addZero(s)}`;
  document.getElementById('minutes').innerText = `${addZero(m)}`;
};

let pauseTimer = () => {
  clearInterval(timerID);
  pauseCount++;
  startBtn.style.display = 'inline-block';
  pauseBtn.style.display = 'none';
  timeList.innerHTML += `<li class='split-row' id='split-row'><span class='time-check'>Pause ${addZero(pauseCount)}:</span> ${addZero(m)}:${addZero(s)}:${addZero(ms)}</li>`;
};

let clearTimer = () => {
  pauseTimer();
  m = s = ms = splitCount = pauseCount = 0;
  updateTimer();
  timeList.innerHTML = '';
};

let splitTime = () => {
  if (m === 0 && s === 0 && ms === 0) return;
  if (startBtn.style.display == 'inline-block') return;
  splitCount++;
  timeList.innerHTML += `<li class='split-row' id='split-row'><span class='time-check'>Split ${addZero(splitCount)}:</span> ${addZero(m)}:${addZero(s)}:${addZero(ms)}</li>`;
};

let m = 0;
let s = 0;
let ms = 0;

let pauseCount = 0;
let splitCount = 0;

let timerID = null;

const startBtn = document.getElementById('start-btn');
const pauseBtn = document.getElementById('pause-btn');
const clearBtn = document.getElementById('clear-btn');
const splitBtn = document.getElementById('split-btn');
const timeList = document.getElementById('split-list');

startBtn.addEventListener('click', startTimer);
pauseBtn.addEventListener('click', pauseTimer);
clearBtn.addEventListener('click', clearTimer);
splitBtn.addEventListener('click', splitTime);